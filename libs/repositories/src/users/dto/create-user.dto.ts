import {IsAlpha, IsEmail, Max} from 'class-validator';

import {User} from '../interfaces/users.interface';

export class CreateUserDto implements User {
  id!: string;

  @IsAlpha()
  description!: string;

  @IsAlpha()
  name!: string;

  @IsEmail()
  email!: string;

  @Max(100, {message: 'custom message: "value" must not exceed 100'})
  value!: number;
}
