import {Inject} from '@nestjs/common';
import {ConfigType, registerAs} from '@nestjs/config';
import {Transport} from '@nestjs/microservices';

export const usersProto = registerAs('USERS_SERVICE', () => ({
  transport: Transport.GRPC,
  options: {
    url: '0.0.0.0:50051',
    package: 'users_service',
    protoPath:
      '/Users/macbbook/Documents/NestJsFastify/nrwl-nestjs/libs/config/grpc/src/proto/users.proto',
  },
}));
export type UsersProto = ConfigType<typeof usersProto>;
export const InjectUsersProto = () => Inject(usersProto.KEY);
