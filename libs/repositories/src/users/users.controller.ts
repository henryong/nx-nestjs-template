import {Controller} from '@nestjs/common';
import {GrpcMethod} from '@nestjs/microservices';

import {CreateUserDto} from './dto/create-user.dto';
import {UserId} from './interfaces';
import {UsersService} from './users.service';

@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @GrpcMethod('UsersService')
  async findUserById(userId: UserId) {
    return this.usersService.findUserById(userId);
  }

  @GrpcMethod('UsersService')
  // eslint-disable-next-line no-empty-pattern
  async findAllUsers({}) {
    return this.usersService.findAllUsers();
  }

  @GrpcMethod('UsersService')
  async createUser(data: CreateUserDto) {
    return this.usersService.createUser(data);
  }
}
