import {fastify} from 'fastify';
import authZeroVerify, {FastifyAuth0VerifyOptions} from 'fastify-auth0-verify';

import {Logger} from '@nestjs/common';
import {ConfigService} from '@nestjs/config';
import {NestFactory} from '@nestjs/core';
import {FastifyAdapter, NestFastifyApplication} from '@nestjs/platform-fastify';

import {AppModule} from './app/app.module';

(async () => {
  const port = 5577;
  const globalPrefix = 'web';
  const server = fastify();
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(server),
  );
  const configService = app.get<ConfigService>(ConfigService);

  app.setGlobalPrefix(globalPrefix);

  server.register(authZeroVerify, <FastifyAuth0VerifyOptions>{
    domain: configService.get<string>('AUTH0_DOMAIN'),
    audience: configService.get<string>('AUTH0_AUDIENCE'),
  });

  await app.listen(port);
  Logger.log(`🚀 Application is running on: http://localhost:${port}/${globalPrefix}`);
})();
