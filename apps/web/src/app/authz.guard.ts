import {FastifyInstance, FastifyReply, FastifyRequest} from 'fastify';

import {
  CanActivate,
  ExecutionContext,
  Injectable,
  Logger,
  OnApplicationBootstrap,
} from '@nestjs/common';
import {HttpAdapterHost} from '@nestjs/core';

enum SERVER_INSTANCE {
  FASTIFY = 'fastify',
}

@Injectable()
export class AuthzGuard implements CanActivate, OnApplicationBootstrap {
  private instance: FastifyInstance;
  private readonly logger = new Logger(AuthzGuard.name);
  constructor(private readonly adapterHost: HttpAdapterHost) {}

  onApplicationBootstrap() {
    if (this.adapterHost.httpAdapter.getType() !== SERVER_INSTANCE.FASTIFY) {
      throw new Error('Custom AuthGuard can only be used with the fastify adapter.');
    }
    this.instance = this.adapterHost.httpAdapter.getInstance();
  }

  async canActivate(context: ExecutionContext) {
    try {
      const request: FastifyRequest = context.switchToHttp().getRequest();
      const reply: FastifyReply = context.switchToHttp().getResponse();

      await this.instance.authenticate(request, reply);

      return true;
    } catch (err) {
      this.logger.error(err);
      return false;
    }
  }
}
