import {Logger, ValidationPipe} from '@nestjs/common';
import {NestFactory} from '@nestjs/core';
import {MicroserviceOptions, Transport} from '@nestjs/microservices';

import {AppModule} from './app/app.module';

(async () => {
  const microserviceOptions: MicroserviceOptions = {
    transport: Transport.GRPC,
    options: {
      url: '0.0.0.0:50051',
      package: 'users_service',
      protoPath:
        '/Users/macbbook/Documents/NestJsFastify/nrwl-nestjs/libs/config/grpc/src/proto/users.proto',
    },
  };
  const app = await NestFactory.createMicroservice(AppModule, microserviceOptions);

  app.useGlobalPipes(new ValidationPipe({transform: true}));
  app.listen();

  Logger.log(`🚀  Microservice application is running`);
})();
