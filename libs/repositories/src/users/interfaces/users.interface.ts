import {DeleteResult} from 'typeorm';

import {CreateUserDto, UpdateUserDto} from '../dto';

export interface UserId {
  id: string;
}
export interface User {
  id: string;
  name: string;
  description: string;
  email: string;
  value: number;
}

export interface IUsersService {
  createUser(user: CreateUserDto): Promise<CreateUserDto>;
  findUserById(id: UserId): Promise<User>;
  // eslint-disable-next-line no-empty-pattern
  findAllUsers({}): Promise<{users: Promise<User[]>}>;
  updateUserById(id: UserId, user: UpdateUserDto): Promise<UpdateUserDto>;
  deleteUserById(id: UserId): Promise<DeleteResult>;
}
