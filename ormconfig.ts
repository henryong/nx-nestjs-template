export const ormconfig = {
  type: 'postgres',
  port: process.env.NODE_ENV !== 'production' 
    ? Number(process.env.POSTGRES_DEV_PORT) || 5432 
    : Number(process.env.POSTGRES_PORT) || 5432,
  host: process.env.POSTGRES_HOST || 'localhost',
  database: process.env.POSTGRES_DATABASE || 'testdb',
  username: process.env.POSTGRES_USERNAME || 'postgres',
  password: process.env.POSTGRES_PASSWORD || 'password',
  logging: true,
  synchronize: process.env.NODE_ENV !== 'production',
  entities: ['libs/repositories/src/**/*.entity.ts'],
  migrations: ['libs/migrations/*.ts'],
  cli: {
    migrationsDir: 'libs/migrations'
  },
};

export default {...ormconfig};