import {Inject, Injectable, Param, ParseUUIDPipe} from '@nestjs/common';
import {CACHE_MANAGER} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {DeleteResult, Repository} from 'typeorm';
import {CreateUserDto, UpdateUserDto} from './dto';
import {Users} from '../users/entities/users.entity';

import {Cache} from 'cache-manager';
import {IUsersService, User} from './interfaces';

@Injectable()
export class UsersService implements IUsersService {
  constructor(
    @InjectRepository(Users) private usersRepo: Repository<Users>,
    @Inject(CACHE_MANAGER) private cache: Cache,
  ) {}

  async createUser(userData: CreateUserDto) {
    const createUserInstance = this.usersRepo.create(userData);
    return this.usersRepo.save(createUserInstance) as Promise<CreateUserDto>;
  }

  async findAllUsers() {
    let allUsers = await this.cache.get('findAllUsers');

    if (!allUsers) {
      allUsers = await this.usersRepo.find();
      this.cache.set('findAllUsers', allUsers, {ttl: 25});
    }

    return {users: allUsers as Promise<User[]>};
  }

  async findUserById(@Param('id', new ParseUUIDPipe({version: '4'})) id: Pick<User, 'id'>) {
    let user = await this.cache.get('findUser');

    if (!user) {
      user = await this.usersRepo.findOneBy(id);
      this.cache.set('findUser', user, {ttl: 10});
    }
    return user as Promise<User>;
  }

  async updateUserById(
    @Param('id', new ParseUUIDPipe({version: '4'})) id: Pick<User, 'id'>,
    userData: UpdateUserDto,
  ) {
    return this.usersRepo.update(id, userData) as Promise<UpdateUserDto>;
  }

  async deleteUserById(@Param('id', new ParseUUIDPipe({version: '4'})) id: Pick<User, 'id'>) {
    return this.usersRepo.delete(id) as Promise<DeleteResult>;
  }
}
