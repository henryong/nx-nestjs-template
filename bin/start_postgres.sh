#!/bin/bash
set -e

CONTAINER_NAME="postgres_database"
USER="postgres"
PW="password"
DB="testdb"

echo "echo stop & remove old docker [$CONTAINER_NAME] and starting new fresh instance of [$CONTAINER_NAME]"
(docker kill $CONTAINER_NAME || :) &&
  (docker volume prune -f || :) &&
  (docker rm $CONTAINER_NAME || :) &&
  docker run --name $CONTAINER_NAME \
    -e POSTGRES_DATABASE=$DB \
    -e POSTGRES_USERNAME=$USER \
    -e POSTGRES_PASSWORD=$PW \
    -p 127.0.0.1:54321:5432 \
    -v postgres_data:/var/lib/postgresql/data \
    -d postgres:14-alpine3.14

# wait for pg to start
echo "sleep wait for pg-server [$CONTAINER_NAME] to start"
SLEEP 3

# create the db
echo "CREATE DATABASE $DB ENCODING 'UTF-8';" | docker exec -i $CONTAINER_NAME psql -U $USER
echo "\l" | docker exec -i $CONTAINER_NAME psql -U $USER
