export default {
  name: 'seed',
  type: 'postgres',
  port: Number(process.env.DATABASE_PORT) || 5432,
  host: process.env.DATABASE_HOST || 'localhost',
  database: process.env.POSTGRES_DATABASE || 'testdb',
  username: process.env.POSTGRES_USERNAME || 'postgres',
  password: process.env.POSTGRES_PASSWORD || 'password',
  logging: true,
  synchronize: false,
  // entities: ['dist/src/**/*.entity.[tj]s'],
  // migrations: ['src/database/seeders/*.[tj]s'],
  // cli: {
  //   migrationsDir: 'src/database/seeders'
  // }
};
