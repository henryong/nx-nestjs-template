import {Get, Post, Body, Param, ParseUUIDPipe} from '@nestjs/common';
import {Controller, Inject, OnModuleInit, UseGuards} from '@nestjs/common';
import {ClientGrpc} from '@nestjs/microservices';
import {CreateUserDto, IUsersService} from '@nrwl-nestjs/repositories';
import {AuthzGuard} from './authz.guard';

@Controller('users')
export class AppController implements OnModuleInit {
  private usersService: IUsersService;
  constructor(@Inject('USERS_SERVICE') private readonly client: ClientGrpc) {}

  onModuleInit() {
    this.usersService = this.client.getService<IUsersService>('UsersService');
  }

  // @UseGuards(AuthzGuard)
  @Get(':id')
  async getUserId(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.usersService.findUserById({id});
  }

  @Get()
  async getAllUsers() {
    return this.usersService.findAllUsers({});
  }

  @Post()
  createUser(@Body() userData: CreateUserDto) {
    return this.usersService.createUser(userData);
  }
}
