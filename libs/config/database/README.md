# libs-config-database

This library was generated with [Nx](https://nx.dev).

## Running unit tests

Run `nx test libs-config-database` to execute the unit tests via [Jest](https://jestjs.io).
