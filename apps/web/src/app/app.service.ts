import {Inject, Injectable} from '@nestjs/common';
import {ClientProxy} from '@nestjs/microservices';
import {CreateUserDto} from '@nrwl-nestjs/repositories';

@Injectable()
export class AppService {
  constructor(@Inject('USERS') private readonly usersClient: ClientProxy) {}

  async getAllUsers() {
    return this.usersClient.send({cmd: 'get_all_users'}, {});
  }

  async getUser(id: string) {
    return this.usersClient.send({cmd: 'get_user'}, id);
  }

  async createUser(userData: CreateUserDto) {
    console.warn('userData service', userData);
    return this.usersClient.send({cmd: 'create_user'}, userData);
  }
}
