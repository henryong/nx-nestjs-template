import {DataSource} from 'typeorm';
import {PostgresConnectionOptions} from 'typeorm/driver/postgres/PostgresConnectionOptions';

import {Logger, Module} from '@nestjs/common';
import {TypeOrmModule} from '@nestjs/typeorm';
import {Users} from '@nrwl-nestjs/repositories';

import {DatabaseConfig, databaseConfig} from '../../src/configurations/database.configuration';

@Module({
  imports: [
    TypeOrmModule.forRootAsync({
      name: 'default', // optional: default value is "default"
      inject: [databaseConfig.KEY],
      useFactory: (dbConfig: DatabaseConfig) =>
        ({
          type: dbConfig.type,
          port: +dbConfig.port,
          host: dbConfig.host,
          database: dbConfig.database,
          username: dbConfig.username,
          password: dbConfig.password,
          logging: dbConfig.logging,
          synchronize: dbConfig.synchronize,
          entities: [Users],
        } as PostgresConnectionOptions),
    }),
  ],
})
export class PostgresDatabaseModule {
  private readonly logger = new Logger(PostgresDatabaseModule.name);
  constructor(private readonly dbConnection: DataSource) {
    if (this.dbConnection.isInitialized) {
      this.logger.log('Database connection established');
    }
  }
}
