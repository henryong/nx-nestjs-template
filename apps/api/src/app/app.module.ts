import {PostgresDatabaseModule, databaseConfig} from '@config/database';
import {RedisCacheModule} from '@config/redis-cache';
import {Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import {UsersModule} from '@nrwl-nestjs/repositories';

@Module({
  imports: [
    ConfigModule.forRoot({isGlobal: true, load: [databaseConfig]}),
    RedisCacheModule,
    PostgresDatabaseModule,
    UsersModule,
  ],
})
export class AppModule {}
