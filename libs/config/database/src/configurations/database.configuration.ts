import postgresormconfig from '@config/orm';
import {Inject} from '@nestjs/common';
import {ConfigType, registerAs} from '@nestjs/config';

export const databaseConfig = registerAs('POSTGRESQL_ORM_CONFIG', () => postgresormconfig);
export type DatabaseConfig = ConfigType<typeof databaseConfig>;
export const InjectDatabaseConfig = () => Inject(databaseConfig.KEY);
