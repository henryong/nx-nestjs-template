import {UsersProto, usersProto} from '@config/grpc';
import {Module} from '@nestjs/common';
import {ConfigModule} from '@nestjs/config';
import {ClientOptions, ClientProxyFactory} from '@nestjs/microservices';

import {AppController} from './app.controller';

@Module({
  imports: [ConfigModule.forRoot({load: [usersProto]})],
  controllers: [AppController],
  providers: [
    {
      provide: 'USERS_SERVICE',
      inject: [usersProto.KEY],
      useFactory: (usersProto: UsersProto) => {
        const clientOptions = usersProto as ClientOptions;
        return ClientProxyFactory.create(clientOptions);
      },
    },
  ],
})
export class AppModule {}
