# ----------------------------------------------------------------
FROM node:16.12.0-alpine3.14
ARG NODE_ENV=production
ENV NODE_ENV=${NODE_ENV}

# Files required by pnpm install
COPY package.json pnpm-lock.yaml ./

# install and cache app dependencies
RUN apk add curl; curl -f https://get.pnpm.io/v6.16.js | node - add --global pnpm
RUN pnpm i --frozen-lockfile	


# add app
COPY ./dist/ ./

# start app
EXPOSE ${PORT}
CMD ["node", "apps/api/main.js"]