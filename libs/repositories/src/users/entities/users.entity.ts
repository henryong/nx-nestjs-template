import {BaseEntity, Column, Entity, PrimaryGeneratedColumn} from 'typeorm';

import {User} from '../interfaces/users.interface';

@Entity('users')
export class Users extends BaseEntity implements User {
  @PrimaryGeneratedColumn('uuid')
  id!: string;

  @Column({type: 'varchar', length: 255})
  name!: string;

  @Column({type: 'varchar', length: 255})
  description!: string;

  @Column({type: 'varchar', length: 255})
  email!: string;

  @Column('int', {default: 0})
  value!: number;
}
